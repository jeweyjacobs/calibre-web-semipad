# Short Notice from the maintainer 

![](semipad.png)

Semi-Pad is a modified version of Calibre-web that adds etherpad pages on the side of the PDF view of books. 

It has been patched together by Martino Morandi and Roel Roscam Abbing as an online library for the [Critical Technical Practice](https://ctp.cc.au.dk/) project of the [DARC](https://darc.au.dk/) research center at Aarhus University.

This modification of [calibre-web](https://github.com/janeczku/calibre-web) wants to facilitate reading groups and collective annotation practices.


